from django.urls import path
from todos.views import todo_list_list, todo_list_details, create_todo_list, update_todo_list, delete_todo_list, create_todo_item, update_todo_item

urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
    path("<int:id>/", todo_list_details, name="todo_list_detail"),
    path("<int:id>/edit/", update_todo_list, name="todo_list_update"),
    path("<int:id>/delete/", delete_todo_list, name="todo_list_delete"),
    path("create/", create_todo_list, name="todo_list_create"),
    path("items/create/", create_todo_item, name="todo_item_create"),
    path("items/<int:id>/edit/", update_todo_item, name="todo_item_update"),
]
