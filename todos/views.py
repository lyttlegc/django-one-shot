from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.
def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    count = TodoList.objects.count()
    context = {
        "todo_object" : todo_lists,
        "detail_count" : count,
    }
    return render(request, "todos/todos.html", context)

def todo_list_details(request, id):
    details = get_object_or_404(TodoList, id=id)
    context = {
        "detail_object" : details,
    }
    return render(request, "todos/detail.html", context)

def create_todo_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()
    context = {
        'form' : form
    }
    return render(request, "todos/create.html", context)

def update_todo_list(request, id):
    todo_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(
            request.POST,
            instance=todo_instance
            )
        if form.is_valid():
            todo_instance = form.save()
            return redirect('todo_list_detail', id=todo_instance.id)
    else:
        form = TodoListForm()
    context = {
        'form' : form
    }
    return render(request, "todos/edit.html", context)

def delete_todo_list(request, id):
    todo_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_instance.delete()
        return redirect('todo_list_list')

    return render(request, "todos/delete.html")

def create_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect('todo_list_detail', id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        'form': form
    }
    return render(request, 'todos/create_items.html', context)

def update_todo_item(request, id):
    todo_instance = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(
            request.POST,
            instance=todo_instance
            )
        if form.is_valid():
            todo_instance = form.save()
            return redirect('todo_list_detail', id=todo_instance.list.id)
    else:
        form = TodoItemForm()
    context = {
        'form' : form
    }
    return render(request, "todos/edit_todo_item.html", context)
